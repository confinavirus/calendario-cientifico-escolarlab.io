= Un aniversario científico para cada día del año.

El Calendario Científico Escolar está dirigido principalmente al alumnado de educación primaria y secundaria obligatoria. Cada día se ha recogido un aniversario científico o tecnológico como, por ejemplo, nacimientos de personas de estos ámbitos o conmemoraciones de hallazgos destacables.

El calendario se acompaña de una guía didáctica con orientaciones para su aprovechamiento educativo transversal en las clases. Estas propuestas didácticas parten de los principios de inclusión, normalización y equidad y se acompañan de pautas generales de accesibilidad. Para ello, se proporcionan tareas variadas que incluyen un amplio rango de habilidades y niveles de dificultad y que, desarrolladas de modo cooperativo, permiten que todo el alumnado haga aportaciones útiles y relevantes. 

Para favorecer su difusión y la utilización en las aulas, todos los materiales están traducidos a varias lenguas: castellano, gallego, euskera, catalán, asturiano, aragonés e inglés.

La información con los aniversarios diarios y las ilustraciones que los acompañan están disponibles en abierto en este repositorio. Además, se pueden descargar el calendario y la guía (maquetados en pdf y texto plano) desde la página web del IGM (http://www.igm.ule-csic.es/calendario-cientifico).

Esta iniciativa pretende contribuir a acercar la cultura científica a la población más joven y crear referentes lo más cercanos posibles para ellos. Por ello, se ha hecho un esfuerzo mayor en dar a conocer personas y hallazgos del presente que constituyan referencias para los jóvenes y, al mismo tiempo, den una visión de dinamismo y actualidad. Se ha prestado especial atención al fomento de un lenguaje no sexista y al aumento de la visibilidad de las mujeres científicas y tecnólogas, para poner a disposición modelos referentes que promuevan las vocaciones científico-técnicas entre las niñas y adolescentes. También se ha puesto un énfasis particular en divulgar la actividad investigadora de los centros públicos españoles.

== Twitter

@CalCientifico

== Telegram

https://t.me/CalendarioCientifico

== Google Calendar

https://calendario-cientifico-escolar.github.io/_/data/ical/2021_es.ical

